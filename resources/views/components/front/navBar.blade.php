<div id="nav">
<b-navbar toggleable="md">
    <b-navbar-toggle target="nav_collapse"></b-navbar-toggle>
    <b-collapse is-nav id="nav_collapse">
        <b-navbar-nav>

            <b-nav-item disabled>
                <router-link to="/" exact style= "color:gray" ><span  :class="{ 'active-button': buttonActive1 }" @click="routerClicked(1)">Home</span></router-link>
            </b-nav-item>
            <b-nav-item disabled>
                <router-link to="/about" exact style= "color:gray"><span :class="{ 'active-button': buttonActive2 }" @click="routerClicked(2)">About</span></router-link>
            </b-nav-item>
            <b-nav-item disabled>
                <router-link to="/project" exact style= "color:gray"><span :class="{ 'active-button': buttonActive3 }" @click="routerClicked(3)">Projects</span></router-link>
            </b-nav-item>
            <b-nav-item disabled>
                <router-link to="/contact" exact style= "color:gray"><span  :class="{ 'active-button': buttonActive4 }" @click="routerClicked(4)">Contact</span></router-link>
            </b-nav-item>
        </b-navbar-nav>
    </b-collapse>
</b-navbar>
</div>
