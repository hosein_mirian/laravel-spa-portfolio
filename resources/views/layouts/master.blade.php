<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Add this to <head> -->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="css/bootstrap-vue.css"/>
    <!-- Add this after vue.js -->
    <script src="js/polyfill.min.js"></script>
    <script src="js/bootstrap-vue.js"></script>

    <title></title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .content {
            text-align: center;
        }

        .active-button {
            color: #ff6600;
        }

        .inactive-button {
            color: gray;
        }

        .footer-div {
            vertical-align: middle;
            text-align: center;
            align-content: center;
            background-color: #cccccc;
            padding-top: 15px;
            height: auto;
            margin: auto;
            width: 100%;
            position: static;
            bottom: 0;

        }

        .centre {
            display: inline-block;
            text-align: center;
        }

    </style>
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 centre">
            <div id="app">
                <div id="navBar">
                    @include('components.front.navBar')
                </div>

                <div id="content">
                    <router-view>

                    </router-view>
                </div>
            </div>
        </div>
    </div>

</div>

{{--@include('components.front.footer')--}}
{{--<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>--}}
<script src="js/app.js"></script>

</body>
</html>
