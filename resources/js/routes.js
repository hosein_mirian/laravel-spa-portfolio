import VueRouter from 'vue-router';
function loadView(view) {
    return () => System.import ( `./components/front/${view}.vue`);
}

let routes = [

    {
        path: '/',
        // component: require('./components/front/Home')
        name: 'Home',
        component: loadView('Home')
    }, {
        path: '/about',
        name: 'About',
        component: loadView('About')
    }, {
        path: '/project',
        name: 'Project',
        component: loadView('Project')
    },{
        path: '/contact',
        name: 'Contact',
        component: loadView('Contact')
    },

];
export default new VueRouter({
    routes,
    mode: 'history'
});