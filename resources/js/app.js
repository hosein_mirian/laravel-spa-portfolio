

require('./bootstrap');
import Vue from 'vue'
import router from './routes'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'

window.Vue = Vue;
Vue.use(VueRouter);
Vue.use(BootstrapVue);
require('./bootstrap-vue.css');
require('./bootstrap.min.css');

/*
require('./polyfill.min');
require('./bootstrap-vue');
*/

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router: router,
    data: {
        buttonActive1: true,
        buttonActive2: false,
        buttonActive3: false,
        buttonActive4: false,
    },
    methods: {
        routerClicked(id) {
            switch (id) {
                case 1:
                    this.buttonActive1 = true;
                    this.buttonActive2 = false;
                    this.buttonActive3 = false;
                    this.buttonActive4 = false;
                    break;
                case 2:
                    this.buttonActive1 = false;
                    this.buttonActive2 = true;
                    this.buttonActive3 = false;
                    this.buttonActive4 = false;

                    break;
                case 3:
                    this.buttonActive1 = false;
                    this.buttonActive2 = false;
                    this.buttonActive3 = true;
                    this.buttonActive4 = false;
                    break;
                case 4:
                    this.buttonActive1 = false;
                    this.buttonActive2 = false;
                    this.buttonActive3 = false;
                    this.buttonActive4 = true;

                    break;
            }
        }
    }



});

