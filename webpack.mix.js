const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');


/**
 * This example will serve files from the './app' directory
 * and will automatically watch for html/css/js changes
 */

var browserSync = require("browser-sync").create();
browserSync.init({
    watch: true,
    proxy:"127.0.0.1:8000",
    files: ['public/**/*.css','public/**/*.js', 'resources/**/*'],
});