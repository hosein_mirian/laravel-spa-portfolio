<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectsType extends Model
{
    public function projects()
    {
        return $this->belongsTo(Projects::class)->select(['id','title']);
    }
}
