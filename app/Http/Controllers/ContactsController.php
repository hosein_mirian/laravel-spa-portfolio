<?php

namespace App\Http\Controllers;

use App\Contacts;
use Illuminate\Http\Request;

class ContactsController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store()
    {
        $this->validate(request(), [
                'name' => 'required',
                'email' => 'required|email',
                'message' => 'nullable'
            ]
        );

        Contacts::forceCreate(
            [
                'name' => request('name'),
                'email' => request('email'),
                'message' => request('message'),
            ]
        );
        return (['message'=>'Message is sent']);
//        return redirect('/contact');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
