<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProjectsType;

class ProjectsController extends Controller
{

    public function index()
    {
        $projects = ProjectsType::with('projects')->latest()->get();
        return $projects;
//        return view('welcome', compact('projects'));
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        $projects = ProjectsType::with('projects')->where('projects_id', $id)->get();
        return $projects;
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
