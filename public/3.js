webpackJsonp([3],{

/***/ 223:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(230)
}
var normalizeComponent = __webpack_require__(228)
/* script */
var __vue_script__ = __webpack_require__(232)
/* template */
var __vue_template__ = __webpack_require__(233)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/front/About.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8bda88e8", Component.options)
  } else {
    hotAPI.reload("data-v-8bda88e8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 227:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(229)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 228:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 229:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 230:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(231);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(227)("8a97b932", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8bda88e8\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./About.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8bda88e8\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./About.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 231:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(13)(false);
// imports


// module
exports.push([module.i, "\n* {\n    outline: none;\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box;\n}\n.container {\n    margin: 0 auto;\n    width: 100%;\n    max-width: 500px;\n}\n.search-box {\n    padding: 10px;\n    margin: 20px 0;\n    border: 1px solid black;\n    border-radius: 5px;\n}\n.timeline {\n    position: relative;\n    border-left: 1px solid #e2dddc;\n}\n.date {\n    display: inline-block;\n    border: 1px solid black;\n    border-radius: 5px;\n    padding: 5px;\n    position: relative;\n    left: 15px;\n    margin: 15px 0;\n}\n.article {\n    position: relative;\n    left: 20px;\n    -webkit-box-shadow: 0px 5px 10px 0px rgba(0, 0, 0, 0.2);\n            box-shadow: 0px 5px 10px 0px rgba(0, 0, 0, 0.2);\n    border-radius: 5px;\n    padding: 10px;\n    margin: 10px 0;\n}\n.article:hover {\n    -webkit-box-shadow: 0px 5px 10px 0px rgba(0, 0, 0, 0.4);\n            box-shadow: 0px 5px 10px 0px rgba(0, 0, 0, 0.4);\n}\na {\n\n    color: #f47b14;\n    text-decoration: none;\n}\n.article-date {\n    font-weight: 300;\n    font-size: 14px;\n}\n.dot {\n    display: block;\n    position: absolute;\n    width: 10px;\n    height: 10px;\n    border-radius: 50%;\n    background: #f5a60c;\n    left: -25.5px;\n    top: calc(50% - 5px);\n}\n\n", ""]);

// exports


/***/ }),

/***/ 232:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = (_defineProperty({
    name: "About",
    mounted: function mounted() {
        console.log('Component mounted.');
    },

    // props: ['datesArticles'],
    data: function data() {
        return {
            searchQuery: ''
        };
    },

    computed: {
        searchedArticles: function searchedArticles() {
            var _this = this;

            var searchRegex = new RegExp(this.searchQuery, 'i');
            var searchedObj = {};

            if (this.searchQuery == '') {
                return this.datesArticles;
            }

            var _loop = function _loop(date) {
                searchedObj[date] = _this.datesArticles[date].filter(function (article) {
                    return searchRegex.test(article.title) || searchRegex.test(article.role) || searchRegex.test(article.description) || searchRegex.test(article.published_at) || searchRegex.test(date);
                });
            };

            for (var date in this.datesArticles) {
                _loop(date);
            }
            return searchedObj;
        }
    },
    methods: {
        anyArticle: function anyArticle() {
            return this.countAllArticles() ? true : false;
        },
        countAllArticles: function countAllArticles() {
            var count = 0;
            for (var date in this.searchedArticles) {
                count += this.searchedArticles[date].length;
            }
            return count;
        }
    }
}, 'data', function data() {
    return {
        datesArticles: {
            'October, 2018': [{
                title: 'KAWeb Co, Lichfield,UK',
                role: 'Laravel and Vue Developer',
                link: 'kaweb',
                description: 'I was cooperating and working on the company’s projects written mostly by Laravel and CodeIgniter along with Vue Js.',
                published_at: '30.09.2016.'
            }, {
                title: 'Birmingham City University - Birmingham,UK',
                role: 'Demonstrator in Computing Faculty',
                link: 'BCU',
                description: 'I am responsible for assisting the lecturers to answer the students\' questions regarding Java Object Oriented Programming. I am working directly with Mr. Andrew Kay and Mr. Nuno Amalio.',
                published_at: '15.09.2016.'
            }],
            'September, 2018': [{
                title: 'Birmingham City University, Birmingham,UK',
                role: 'Graduate MSc Advanced Computer Science with a 2.1 Degree',
                link: 'MSc',
                description: 'I finished my MSc Advanced Computer Science with a first grade from Birmingham City University.',
                published_at: '15.09.2016.'
            }, {
                title: 'GravitiChain Co, Birmingham,UK',
                role: 'React Native Developer',
                link: 'graviti',
                description: 'GravitiChain is a young startup company based in Birmingham working around Block Chain technology and banking systems. I worked as the react native developer to produce the native application which is a CRM system, fully compatible with all mobile platforms.\n' + 'I Used React Native, AMAZON AWS, JavaScript, PHP and MYSQL to create the CRM service. ',
                published_at: '02.04.2016.'
            }],
            'September 2016 to 2017': [{
                title: 'English With Smart subtitle App, Iran',
                role: 'Android Developer',
                link: 'smartsub',
                description: 'his was a complete innovative idea formed in my mind and I implemented it independently because I believed I could make an App to teach all the world English using movies with subtitle that is more intelligence compare to the simple one. ',
                published_at: '25.12.2015.'
            }], 'January 2014 to January 2016': [{
                title: 'KDG-Food, Iran',
                role: 'Android Developer',
                link: 'kdg',
                description: 'KDG-FOOD is a food distribution company in Esfahan, Iran. The company goal was dominating the market by using android apps and increase its income consequently. I designed and created six cutting-edge mobile apps by using the latest technologies to meet my Managers demand. It was a tough joyful challenge as I was handling a three members’ team to do this job. One of the applications my team made was a Management app specialized for the manager to watch out and manage goods, prices, statistics, etc.\n' + '❖ The other apps we made, was an intelligence system which offers customers the best sellers products automatically; We designed many algorithms and used numerous strategies to process statistic data in order to filter these best sellers’ ones.\n' + '❖ Making profit by advertising through apps like a slideshow and video advertising was one of my achievement for the company.',
                published_at: '25.12.2015.'
            }]
        }
    };
}));

/***/ }),

/***/ 233:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: " bg-white box-shadow" }, [
    _vm._m(0),
    _vm._v(" "),
    _vm.anyArticle()
      ? _c(
          "div",
          { staticClass: "timeline m-5 " },
          _vm._l(_vm.searchedArticles, function(dateWithArticles, date) {
            return _c(
              "div",
              [
                dateWithArticles.length > 0
                  ? _c("p", { staticClass: "date" }, [_vm._v(_vm._s(date))])
                  : _vm._e(),
                _vm._v(" "),
                _vm._l(dateWithArticles, function(article) {
                  return _c("div", { staticClass: "article " }, [
                    _c("span", { staticClass: "dot" }),
                    _vm._v(" "),
                    _c("h5", [
                      _c("a", { attrs: { href: "#" + article.link } }, [
                        _vm._v(_vm._s(article.title))
                      ])
                    ]),
                    _vm._v(" "),
                    _c("h3", [_vm._v(_vm._s(article.role))]),
                    _vm._v(" "),
                    _c("p", { staticStyle: { "text-align": "center" } }, [
                      _vm._v(_vm._s(article.description))
                    ])
                  ])
                })
              ],
              2
            )
          })
        )
      : _c("p", [_vm._v("No articles found.")])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row m-3 " }, [
      _c("div", { staticClass: "col-lg-6 mx-auto text-center mt-3" }, [
        _c("h5", { staticStyle: { "text-align": "left" } }, [
          _vm._v("A little About me")
        ]),
        _vm._v(" "),
        _c(
          "p",
          { staticStyle: { "text-align": "justify", "font-size": "20pt" } },
          [
            _vm._v(
              "\n                Full Stack Developer / Android Developer / React Native / English Tutor\n            "
            )
          ]
        ),
        _vm._v(" "),
        _c("p", { staticStyle: { "text-align": "justify" } }, [
          _vm._v(
            "Graduate MSc student of Birmingham City University in England with huge practical experience in\n                JAVA, PHP, CodeIgniter, React Native and MYSQL. Possesses skills and efficiency in working\n                on different platforms and domains. A confident and enthusiastic individual with excellent\n                communication and problem-solving skills and a team player. Developed several Android applications\n                using Eclipse & IntelliJ IDEA and java. Great ability to design database and graphics interface from\n                scratch and a reliable, responsible and well-experienced person with more than 10 years involvement\n                and working in different positions.\n            "
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-lg-6 text-center mt-3" }, [
        _c("img", {
          attrs: { src: "img/profile.jpg", width: "300px", height: "300px" }
        })
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-8bda88e8", module.exports)
  }
}

/***/ })

});