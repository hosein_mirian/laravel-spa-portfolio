webpackJsonp([1],{

/***/ 225:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(238)
}
var normalizeComponent = __webpack_require__(228)
/* script */
var __vue_script__ = __webpack_require__(240)
/* template */
var __vue_template__ = __webpack_require__(241)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/front/Home.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1d151330", Component.options)
  } else {
    hotAPI.reload("data-v-1d151330", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 227:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(229)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 228:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 229:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 238:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(239);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(227)("072d6402", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1d151330\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Home.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1d151330\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Home.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 239:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(13)(false);
// imports


// module
exports.push([module.i, "\n.centre {\n    display: inline-block;\n    text-align: center;\n}\n.box-shadow {\n    -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, .2);\n            box-shadow: 0 2px 5px rgba(0, 0, 0, .2);\n}\n.bg-white {\n    background: white;\n}\n#socialMedia div{\n    margin-bottom: 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 240:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "Home"
});

/***/ }),

/***/ 241:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: " bg-white box-shadow" }, [
    _c("div", { staticClass: "row m-3 " }, [
      _c("div", { staticClass: "col-lg-6  mt-3" }, [
        _c("h3", { staticStyle: { "text-align": "left" } }, [
          _vm._v("Programming Skills")
        ]),
        _vm._v(" "),
        _vm._m(0),
        _vm._v(" "),
        _c(
          "div",
          {
            staticStyle: { "text-align": "left" },
            attrs: { id: "socialMedia" }
          },
          [
            _c("div", [
              _c(
                "a",
                {
                  staticStyle: { "font-size": "12pt" },
                  attrs: {
                    href: "https://gitlab.com/users/hosein_mirian/projects",
                    target: "_blank"
                  }
                },
                [
                  _c("img", {
                    attrs: {
                      src: __webpack_require__(242),
                      width: "10%",
                      height: "10%"
                    }
                  }),
                  _vm._v("\n                        My GitLab")
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", [
              _c(
                "a",
                {
                  staticStyle: { "font-size": "12pt" },
                  attrs: {
                    href: "https://www.linkedin.com/in/hossein-mirian",
                    target: "_blank"
                  }
                },
                [
                  _c("img", {
                    attrs: {
                      src: __webpack_require__(243),
                      width: "10%",
                      height: "10%"
                    }
                  }),
                  _vm._v("\n                        My LinkedIn")
                ]
              )
            ])
          ]
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "col-lg-6  mt-3" },
        [
          _c(
            "b-card-group",
            { attrs: { columns: "" } },
            [
              _c(
                "b-card",
                {
                  attrs: {
                    title: "Vue",
                    "img-src": "img/vue.png",
                    "img-fluid": "",
                    "img-alt": "image",
                    "img-top": ""
                  }
                },
                [
                  _c("p", { staticClass: "card-text" }, [
                    _vm._v(
                      "\n                        Vue Js is one of the frameworks I am very interested to use in my projects due to\n                        its simplicity of use.\n                    "
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "b-card",
                {
                  attrs: {
                    title: "React",
                    "img-src": "img/react.png",
                    "img-fluid": "",
                    "img-alt": "image",
                    "img-top": ""
                  }
                },
                [
                  _c("p", { staticClass: "card-text" }, [
                    _vm._v(
                      "\n                        React Native and React js are my favorite frameworks to work on either Mobile or Website\n                        development.\n                    "
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "b-card",
                {
                  attrs: {
                    title: "JavaScript",
                    "img-src": "img/js.png",
                    "img-fluid": "",
                    "img-alt": "image",
                    "img-top": ""
                  }
                },
                [
                  _c("p", { staticClass: "card-text" }, [
                    _vm._v(
                      "\n                        Knowing Javascript is a must and after releasing new JS6 I have equipped myself with the\n                        latest changes in Javascript\n                    "
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "b-card",
                {
                  attrs: {
                    title: "PHP",
                    "img-src": "img/php.png",
                    "img-fluid": "",
                    "img-alt": "image",
                    "img-top": ""
                  }
                },
                [
                  _c("p", { staticClass: "card-text" }, [
                    _vm._v(
                      "\n                        Php is my favorite back-end framework I have been using it in my Mobile and Web development.\n                        I am also totally expertise with PHP frameworks such as Laravel and Codeigniter.\n                    "
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "b-card",
                {
                  attrs: {
                    title: "MySQL",
                    "img-src": "img/mysql.png",
                    "img-fluid": "",
                    "img-alt": "image",
                    "img-top": ""
                  }
                },
                [
                  _c("p", { staticClass: "card-text" }, [
                    _vm._v(
                      "\n                        Applying MySql queries is one of the other skills I have achieved by working on various\n                        projects related to databases.\n                    "
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "b-card",
                {
                  attrs: {
                    title: "Java",
                    "img-src": "img/java.png",
                    "img-fluid": "",
                    "img-alt": "image",
                    "img-top": ""
                  }
                },
                [
                  _c("p", { staticClass: "card-text" }, [
                    _vm._v(
                      "\n                        Java is a fantastic language I mostly use for developing Android apps using Android Studio.\n                    "
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "b-card",
                {
                  attrs: {
                    title: "BootStrap",
                    "img-src": "img/boot.png",
                    "img-fluid": "",
                    "img-alt": "image",
                    "img-top": ""
                  }
                },
                [
                  _c("p", { staticClass: "card-text" }, [
                    _vm._v(
                      "\n                        Nowadays users would like to see the websites in a shaped and mobile friendly way. I use\n                        bootstrap to have responsive apps.\n                    "
                    )
                  ])
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticStyle: { "text-align": "justify" } }, [
      _vm._v(
        " As a Full-Stack developer I am\n                obliged to work with variety of technologies such as Vue, React Js, React Native, Java Script to\n                develop\n                my front end side of websites while for developing Back-End I normally use PHP and MySQL. I am also\n                very experienced\n                with PHP framework such as CodeIgniter and Laravel.\n                "
      ),
      _c("br"),
      _vm._v(" "),
      _c("br"),
      _vm._v(
        "\n                Apart from web development. I have a great experience developing Android using Java and React Native\n                as well.\n            "
      )
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1d151330", module.exports)
  }
}

/***/ }),

/***/ 242:
/***/ (function(module, exports) {

module.exports = "/images/gitlabicon.png?af441ebd7b25b31d22d783cac3e3e949";

/***/ }),

/***/ 243:
/***/ (function(module, exports) {

module.exports = "/images/linkedinicon.png?bbd6e88cb1b8b3a02e3b085320a6bced";

/***/ })

});