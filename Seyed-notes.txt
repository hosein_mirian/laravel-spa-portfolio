
Creating Model and migration:
php artisan make:model {table_name}: -m
———————

Create table and Migration

php artisan make:model {table_name}: -m
php artisan make:migration create_users_table --create=users

———————
Database or Migration Errors

Composer dump-autoload

———————
Reset Migration

Php artisan migrate:reset

———————
Migrate the new file
Php artisan migrate

Php artisan roleback  — undo the last change
————————
Make model and controller together with migration ***
Php artisan make:model Post -mc

——*** important make the function by using -r
Php artisan make:controller TasksController -r

// do this when u change something in the middle of migration and need to fresh the database. —seed fill the database with fake records. 
php artisan migrate:fresh --seed
php artisan db:seed --class="DevDatabaseSeeder"

////////========== Git to the dev branch
 git branch
git status
git add {file names—red ones}
git commit
git push origin {branch name}

— After approving… 
Git checkout develop — switch to develop branch
git pull origin develop 
git checkout -b {name}


php artisan cache:clear
php artisan config:cache