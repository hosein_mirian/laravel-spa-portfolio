<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/project', 'ProjectsController@index');
Route::get('/project/{id}', 'ProjectsController@show');

Route::post('/contact', 'ContactsController@store');



//Route::get('/project', function () {
//    return App\ProjectsType::with('projects')->latest()->get();
//});

//Route::get('/posts/create', 'PostsController@create');
//Route::post('/posts', 'PostsController@store');
//Route::get('/posts/{post}', 'PostsController@show');

/*The next step is that we need to adjust
the Laravel routing a little bit. Open
the app/Http/routes.php file and add the
following lines to your project. This will force Laravel
to not reload the application on every route change. Laravel’s
routing is now stopped and VueJS can take it over from here.
Route::get('/vue/{vue_capture?}', function () {
    return view('vue.index');
})->where('vue_capture', '[\/\w\.-]*');*/